package com.example.asyncconection;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.asyncconection.Network.Conexion;
import com.example.asyncconection.Network.Resultado;
import com.example.asyncconection.databinding.ActivityMainBinding;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private ActivityMainBinding binding;
    private static final int REQUEST_CONNECT =1;
    long inicio, fin;
    TareaAsincrona tareaAsincrona;
    URL url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        binding.button.setOnClickListener((View.OnClickListener) this);
    }

    @Override
    public void onClick(View v){
        try{
            url = new URL(binding.editTextTextPersonName.getText().toString());
            if(binding.switch1.isChecked()){
                descargaOkHTTP(url);
            }
            else{
                descarga(url);
            }
        }catch (MalformedURLException e){
            e.printStackTrace();
            mostrarError(e.getMessage());
        }

    }

    private void descarga(URL url) {
        inicio = System.currentTimeMillis();
        tareaAsincrona = new TareaAsincrona(this);
        tareaAsincrona.execute(url);
        binding.textView2.setText("Descargando la pagina ...");
    }

    private void descargaOkHTTP(URL web) {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(web)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try(ResponseBody responseBody = response.body()){
                    if(response.isSuccessful()){
                        final String responseData = response.body().string();
                        mostrarRespuesta(responseData);
                    }
                    else{
                        mostrarRespuesta("Unexpected code: " + response);
                    }

                }

            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.e("Error", e.getMessage());
                mostrarRespuesta("Fallo: " + e.getMessage());
                
            }
        });

    }

    private void mostrarRespuesta(String s) {
        fin = System.currentTimeMillis();
        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                binding.webView.loadDataWithBaseURL(String.valueOf(url), s,"text/html","UTF-8", null);
                binding.textView2.setText("Duración: " + String.valueOf(fin-inicio) + " ms");
            }
        });
    }

    private void mostrarError(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT);
    }

    public class TareaAsincrona extends AsyncTask<URL, Void, Resultado>{
        private ProgressDialog progreso;
        private Context context;

        public TareaAsincrona (Context context){
            this.context=context;
        }

        @Override
        protected Resultado doInBackground(URL... urls) {
            Resultado resultado;
            try{
                resultado = Conexion.conectarJava(urls[0]);
            }catch (IOException e){

                e.printStackTrace();
                Log.e("error de conexión : ", e.getMessage());
                resultado = new Resultado();
                resultado.setCodigo(500);
                resultado.setMensaje(e.getMessage());
            }
            return resultado;
        }

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            progreso = new ProgressDialog(context);
            progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progreso.setMessage("Conectando ...");
            progreso.setCancelable(true);
            progreso.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    TareaAsincrona.this.cancel(true);
                }
            });
            progreso.show();
        }

        @Override
        protected void onPostExecute(Resultado resultado){
            super.onPostExecute(resultado);
            progreso.dismiss();
            fin = System.currentTimeMillis();
            if(resultado.getCodigo() == HttpURLConnection.HTTP_OK){
                binding.webView.loadDataWithBaseURL(String.valueOf(url), resultado.getContenido(),"text/html","UTF-8", null);
            }else {
                mostrarError(resultado.getMensaje());
                binding.webView.loadDataWithBaseURL(String.valueOf(url), resultado.getMensaje(),"text/html","UTF-8", null);

            }
            binding.textView2.setText("Duracion: " + String.valueOf(fin - inicio) + "ms");
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            progreso.dismiss();
            mostrarError("Cancelado");
        }
    }
}